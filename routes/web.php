<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Auth::routes();

Route::group(['middleware' => ['auth']], function() {
    Route::get('/dashboard', 'HomeController@index')->name('dashboard');
    Route::get('/charts', 'ChartController@index')->name('charts');
    Route::get('/tables', 'TableController@index')->name('tables');
    Route::get('/map', 'MapController@index')->name('map');

    Route::name('components.')->group(function () {
        Route::get('/navbar', 'ComponentsController@navbar')->name('navbar');
        Route::get('/cards', 'ComponentsController@cards')->name('cards');
    });

    Route::post('/save_marker_ajax','MapController@save_marker_ajax')->name('save_marker_ajax');
});