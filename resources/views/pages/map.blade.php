@extends('layouts.app')

@section('content')

    <div class="row">
        <input class="form-control col-md-8" type="text" id="address_auto">
        <button class="btn bnt-primary" id="save_marker">Save</button>
    </div>
    <div class="row">
        <div class="map-location">
            <div id="map"></div>
        </div>
        <div class="col-md-8 col-list">
            <h5 class="">List of markers in DB:</h5><br>

            <ul class="list-group" id="maker-list">
                @forelse($markers as $marker)
                    <li>latitude: {{ $marker->latitude }}, longitude: {{ $marker->longitude }} </li>
                @empty
                    {{-- --}}
                @endforelse
            </ul>
        </div>
    </div>

    <input type="hidden" name="lat" id="lat">
    <input type="hidden" name="lng" id="lng">
@push('scripts')
    <script>
        function initMap() {
            var map = new google.maps.Map(document.getElementById('map'), {
                center: {lat: -34.397, lng: 150.644},
                zoom: 8
            });

            var marker = new google.maps.Marker({
                map: map,
                draggable: true
            });


            var lat = document.getElementById("lat");
            var lng = document.getElementById("lng");

            var input = document.getElementById('address_auto');
            var autocomplete = new google.maps.places.Autocomplete(input);
            autocomplete.bindTo('bounds', map);


            google.maps.event.addListener(autocomplete, 'place_changed', function () {
                var place = autocomplete.getPlace();

                marker.setPosition(place.geometry.location);
                lat.value = place.geometry.location.lat();
                lng.value = place.geometry.location.lng();

                //set zoom on map after autocomplete
                var pt = new google.maps.LatLng(lat.value, lng.value);
                map.setCenter(pt);
                map.setZoom(10);
            });
            var marker;
            var geocoder = new google.maps.Geocoder();

            marker = new google.maps.Marker({
                map: map,
                draggable: true
            });

            var lat = document.getElementById("lat");
            var lng = document.getElementById("lng");

            var input = document.getElementById('address_auto');
            var autocomplete = new google.maps.places.Autocomplete(input);
            autocomplete.bindTo('bounds', map);

            // for future
//            google.maps.event.addListener(map, 'click', function (event) {
//                marker.setPosition(event.latLng);
//            });

            google.maps.event.addListener(autocomplete, 'place_changed', function () {
                var place = autocomplete.getPlace();

                marker.setPosition(place.geometry.location);
                lat.value = place.geometry.location.lat();
                lng.value = place.geometry.location.lng();

                //set zoom on map after autocomplete
                var pt = new google.maps.LatLng(lat.value, lng.value);
                map.setCenter(pt);
                map.setZoom(10);
            });


            $("#save_marker").click(function () {
                if (lat.value !== '' && lng.value !== '') {
                    $.ajax({
                        url: '{{ route('save_marker_ajax') }}',
                        method: 'post',
                        data: {
                            _token: '{{csrf_token()}}',
                            lat: lat.value,
                            lng: lng.value
                        },
                        success: function (response) {
                            if (response.latitude) {
                                $('#maker-list').append('<li>latitude:' + response.latitude + ', longitude: ' + response.longitude + '</li>');

                                lat.value = '';
                                lng.value = '';
                            }
                        },
                        error: function (request) {
                            alert(request.responseJSON.error);
                        }
                    })
                }
            });


        }
    </script>
    <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDVT51qE2E5QhjGAcmbadNMxAk5Nf8cOo0&callback=initMap&libraries=places&language=en"></script>
    @endpush
    @push('styles') {{-- TODO:: move to styles--}}
    <style>
        html, body {
            height: 100%;
            margin: 0;
            padding: 0;
        }

        .map-location {
            position: relative;
            width: 100%;
            height: 500px;
        }
        #map{
            height: 500px;
        }
        .col-list {
            max-height: 200px;
            overflow-x: hidden;
        }
        @me
    </style>
    @endpush

@endsection
