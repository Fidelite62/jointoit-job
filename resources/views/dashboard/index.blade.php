@extends('layouts.app')

@section('content')

    @include('layouts.breadcrumbs')

    <!-- Icon Cards-->
    @include('dashboard.icons')

    <!-- Area Chart Example-->
    @include('dashboard.charts')


    <!-- DataTables Example -->
    @include('dashboard.DataTables')

@endsection
