@extends('layouts.guestApp')

@section('content')
    <div class="container">
        <div class="card mx-auto mt-5">
            <div class="card-header">{{ __('Login') }}</div>
            <div class="card-body">
                @if ($errors->any())
                    {!! implode('', $errors->all('<div class="pb-3">:message</div>')) !!}
                @endif

                <form method="POST" action="{{ route('login') }}">
                    @csrf
                    <div class="form-group">
                        <div class="form-label-group">
                            <input id="inputEmail" type="email" name="email"
                                   class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                                   value="{{ old('email') }}" required autofocus>
                            <label for="inputEmail">Email address</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="form-label-group">
                            <input type="password" id="inputPassword" name="password"  class="form-control" placeholder="Password"
                                   required>
                            <label for="inputPassword">Password</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" value="remember-me" name="remember" {{ old('remember') ? 'checked' : '' }}>
                                Remember Password
                            </label>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary">
                    {{ __('Login') }}
                    </button>
                </form>
                <div class="text-center">
                    <a class="d-block small mt-3" href="{{ route('register') }}">
                        Register an Account
                    </a>
                    <a class="d-block small" href="{{ route('password.request') }}">
                    {{ __('Forgot Your Password?') }}
                    </a>
                </div>
            </div>
        </div>
    </div>
@endsection
