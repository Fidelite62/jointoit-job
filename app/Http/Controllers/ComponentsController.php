<?php

namespace App\Http\Controllers;

class ComponentsController extends Controller
{
    /**
     * ComponentsController constructor.
     */
    function __construct()
    {
        $this->middleware('permission:components-view');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function navbar()
    {
        return view('pages.navbar');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function cards()
    {
        return view('pages.cards');
    }
}
