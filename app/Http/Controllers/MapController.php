<?php

namespace App\Http\Controllers;

use App\Map;
use Illuminate\Http\Request;

class MapController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:charts-view');
        $this->middleware('permission:create-markers', ['only' => ['save_marker_ajax']]);
    }

    public function index()
    {
        $markers = Map::get();

        return view('pages.map')->with('markers',$markers);
    }

    public function save_marker_ajax(Request $request)
    {
        $map = new Map();
        $data=[];

        if($request->has('lat') && $request->has('lng')){
            if($request->lat !==null){
                $data['latitude'] = $request->lat;
                $data['longitude'] = $request->lng;
                $map->fill($data);
                $map->save();
            }
        }

        return response()->json($data);
    }
}
