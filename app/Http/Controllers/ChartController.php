<?php

namespace App\Http\Controllers;

use App\Chart;
use Illuminate\Http\Request;

class ChartController extends Controller
{
    /**
     * ChartController constructor.
     */
    function __construct()
    {
        $this->middleware('permission:charts-view');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('pages.charts');
    }
}
