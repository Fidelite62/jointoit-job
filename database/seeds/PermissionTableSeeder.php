<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [
            'dashboard-view',
            'charts-view',
            'components-view',
            'tables-view',
            'create-markers'
        ];

        foreach ($permissions as $permission)
            Permission::create([ 'name' => $permission ]);

// Roles
        $root = Role::findByName('root');
        $admin = Role::findByName('admin');
        $manager = Role::findByName('manager');
        $user = Role::findByName('user');

        $adminPermissions = [
            'dashboard-view',
            'charts-view',
            'components-view',
            'create-markers'
        ];

        $managerPermissions = [
            'tables-view',
        ];

        $userPermissions = [
            'components-view',
        ];

        foreach ($permissions as $permission)
            $root->givePermissionTo($permission);

        foreach ($adminPermissions as $permission)
            $admin->givePermissionTo($permission);

        foreach ($managerPermissions as $permission)
            $manager->givePermissionTo($permission);

        foreach ($userPermissions as $permission)
            $user->givePermissionTo($permission);
    }

}
