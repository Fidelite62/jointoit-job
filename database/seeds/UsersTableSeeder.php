<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $superAdmin = User::create([
            'name'     => 'Super man',
            'email'    => 'superAdmin@gmail.com',
            'password' => bcrypt('secret'),
        ]);
        $superAdmin->assignRole('root');

        $admin = User::create([
            'name'     => 'Insane admin',
            'email'    => 'admin@gmail.com',
            'password' => bcrypt('secret'),
        ]);
        $admin->assignRole('admin');

        $manager = User::create([
            'name'     => 'Biba',
            'email'    => 'manager@gmail.com',
            'password' => bcrypt('secret'),
        ]);
        $manager->assignRole('manager');

        $user = User::create([
            'name'     => 'Boba',
            'email'    => 'user@gmail.com',
            'password' => bcrypt('secret'),
        ]);
        $user->assignRole('user');
    }
}
